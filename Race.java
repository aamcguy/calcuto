protected class Race {
    protected String name;
    protected double attackTime = 1;
    protected double birthRate = 1;
    protected double buildingCost = 1;
    protected double beExtra = 0;
    protected double gains = 1;
    protected double magicEff = 1;
    protected double thiefEff = 1;
    protected double offLosses = 1;
    protected double defLosses = 1;
    protected double enemyLossesAttacking = 1;
    protected double maxPop = 1;
    protected double sciEff = 1;
    protected double draftCost = 1;
    protected double foodConsumption = 1;
    
    protected boolean hasPlague = 0;
    protected boolean hasStables = 1;
    protected boolean creditsBuild = 1;

    //Military Units
    protected Soldier soldier;
    protected Ospec ospec;
    protected Dspec dspec;
    protected Elite elite;

    public Race {
    }


    /* The following classes create the military units each race has:
     * Soldier, Offensive Specialist, Defensive Specialist, Elite 
     */
    protected class Soldier implements Military {
       String name;
       int offPower;
       int defPower;
       double nw;
       int cost;

       protected Soldier(String name, int off, int def) {
           this.name = new String(name);
           this.offPower = off;
           this.defPower = def;
           this.cost = 0;
           //As of right now, soldiers have 1.5 nw for all races
           nw = 1.5;
       }
    }
    protected class Ospec implements Military {
       String name;
       int offPower;
       int defPower;
       double nw;
       int cost;
       
       protected Ospec(String name, int off, int def, int cost) {
           this.name = new String(name);
           this.offPower = off;
           this.defPower = def;
           this.cost = cost;
           //networth of ospec is 80% of its offensive value
           nw = offPower * 0.8;
       }
    }
    protected class Dspec implements Military {
       String name;
       int offPower;
       int defPower;
       double nw;
       int cost;

       protected Dspec(String name, int off, int def, int cost) {
           this.name = new String(name);
           this.offPower = off;
           this.defPower = def;
           this.cost = cost;
           //networth of ospec is 100% of its defensive value
           nw = defPower * 1;
       }
    }
    protected class Elite implements Military {
       String name;
       int offPower;
       int defPower;
       double nw;
       int cost;

       protected Elite(String name, int off, int def, int nw, int cost) {
           this.name = new String(name);
           this.offPower = off;
           this.defPower = def;
           this.nw = nw;
           this.cost = cost;
       }
    }
}
