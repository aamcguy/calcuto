public class Avian extends Race {
    public Avian {
        super();
        
        name = new String("Avian");
        //Update Race-specific bonuses/penalties
        attackTime = 0.7;
        birthRate = 1.2;
        gains = 0.9;
        hasStables = 0;

        //Create Military Units
        soldier = new Soldier("Soldiers", 1, 1);
        ospec = new Ospec("Griffins", 4, 0, 350);
        dspec = new Dspec("Harpies", 0, 4, 350);
        elite = new Elite("Drakes", 6, 2, 5, 500);
    }
}
