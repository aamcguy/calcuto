public class Elf extends Race {
    public Elf() {
        super();
        name = new String("Elf");
        //Racial Bonuses/Penalties
        magicEff = 1.3;
        defLosses = 0.5;

        //Military Units
        soldier = new Soldier("Soldiers", 1, 1);
        ospec = new Ospec("Rangers", 4, 0, 350);
        dspec = new Dspec("Archers", 0, 5, 350);
        elite = new Elite("Elf Lords", 5, 4, 5.75, 800);
    }

}
