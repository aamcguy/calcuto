public class Dwarf extends Race {
    public Dwarf {
        super();

        name = new String("Dwarf");
        // Province Bonuses/Penalties
        beExtra = 0.3;
        buildingCost = 0;
        creditsBuild = 0;
        foodConsumption = 1.75;

        //Create Military Units
        soldier = new Soldier("Soldiers", 1, 1);
        ospec = new Ospec("Warriors", 4, 0, 350);
        dspec = new Dspec("Axemen", 0, 4, 350);
        elite = new Elite("Berserkers", 6, 3, 5.5, 700);
    }
}
