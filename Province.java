/* This class creates a representation of a Utopian province.  It contains all
 * attributes that are common across all races in Utopia.  This class should
 * only be updated if core province attributes are changed.  All race changes should
 * be changed in the corresponding race class.
 */

public class Province {
    //meta
    protected String provName =  new String("Province");
    protected String rulerName = new String("Ruler");
    protected String race;
    protected String pers;
    
    //resources
    protected int acres;
    protected int gc;
    protected int food;
    protected int runes;
    protected long tb;
    protected int nw;
    
    //population
    protected int pop;
    protected int draftTarget;
    protected int peasants;
    protected int soldiers;
    protected int ospecs;
    protected int dspecs;
    protected int elites;
    protected int thieves;
    protected int wizards;
    protected int horses;
    protected int prisoners;
}
